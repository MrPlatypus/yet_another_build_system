PATH_ROOT="$(realpath "$(dirname "$BASH_SOURCE")")"
pushd $PATH_ROOT 1>/dev/null
mkdir -p ./__build__/debug
gcc -c -w -g -DMTR_ENABLED -DVERBOSE -I. -o ./__build__/debug/main.o ./src/main.c & \
gcc -c -w -g -DMTR_ENABLED -DVERBOSE -I. -o ./__build__/debug/file_system.linux.o ./src/file_system.linux.c & \
gcc -c -w -g -DMTR_ENABLED -DVERBOSE -I. -o ./__build__/debug/system.linux.o ./src/system.linux.c & \
gcc -c -w -g -DMTR_ENABLED -DVERBOSE -I. -o ./__build__/debug/process.linux.o ./src/process.linux.c & \
gcc -c -w -g -DMTR_ENABLED -DVERBOSE -I. -o ./__build__/debug/thread.linux.o ./src/thread.linux.c & \
gcc -c -w -g -DMTR_ENABLED -DVERBOSE -I. -o ./__build__/debug/minitrace.o ./external/minitrace/minitrace.c
pushd ./__build__/debug/ 1>/dev/null
gcc -o YABS.out main.o file_system.linux.o system.linux.o process.linux.o thread.linux.o minitrace.o -lpthread
cp YABS.out ../../YABS.out
popd 1>/dev/null
popd 1>/dev/null