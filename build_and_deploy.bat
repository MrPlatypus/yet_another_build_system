@ECHO off

SET PATH_ROOT=%~dp0

pushd %PATH_ROOT%

call build_msvc.bat
"%PATH_ROOT%__build__/debug/YABS.exe" data/YABS.windows.release.build
copy /Y "%PATH_ROOT%__build__\release\YABS.exe" "%PATH_ROOT%YABS.exe"

popd