#### What is this ?
A pretty straightforward build system. It's got profiling and the build steps can be multithreaded, which was all I need for now.   
#### But why ?
Just for fun. **YABS** is no more than a command sequencer so you could use it for things other than compiling if you wanted to.
#### Build
###### Windows
All you need to build is a **Developer Command Prompt for Visual Studio** (tested with 2017, 2019, 2022) and call **build\_msvc.bat**.
This will generate a non optimised executable with debug symbols.  
###### Linux
As long as you have clang installed you can run the build_linux.sh script in your terminal   
######   
You can now have fun using the **YABS** binary copied at the root of the project to compile **YABS** again in either debug or release modes.   
#### How to use ?
YABS CONFIG_FILE [--force, --check]   
Pass your config.build file (the .build extension is not mandatory) which can then be followed by either --force or --check   

<ins>ex</ins>:   
./YABS ./data/YABS.windows.release.build   
(For this example to work you would have to copy the binary outside of the **\_\_build\_\_** directory)

#### The .build file format
**.build** files are **.csv** files, composed of 5 columns   
**Step Title**;**Task Title**;**Dependencies**;**Command**;**Argument**  
- **Step Title** is used to determine which tasks can be executed at the same time. If this field is left empty then we consider that you are still working on whatever the previous **Step Title** was. Please note that the first line **must** have a **Step Title**.   
- **Task Title** is mainly used for adding more detail to the generated graph. If this field is left empty then the **Step Title** will be used instead.   
- **Dependencies** is a list of ',' seperated file paths. If the field is empty then the task will always be executed, if it isn't then each file will be checked for it's existence and it's timestamp. Any file that changed since last running this particular .build file will cause the task to be executed.   
- **Command** if the task is run this is the command that will be executed as a sub process (Take in account that relative paths will start from wherever you called YABS.exe). This field can be left empty and in that case the previously used **Command** will be used (check the next column to see how to best take advantage of this)   
- **Argument** if this value is specified it will be concatanated at the end of the command string. This can be useful when reusing the **Command** field.    

In case you wish to force all steps to run no matter what, pass the **"--force"** param after the path to your **.build** file.  
You can also use the **"--check"** param to verify your .build file, please note that this will be done automatically once whenever you modify your .build file.
This file format was not chosen to be easily readable but you can make the experience a bit more pleasing by using a spreadsheet tool to read and edit the .csv.
Here is a simple example of what a **.build** file could look like (The columns have been vertically aligned to simplify the reading).   
```
Setup Build Environment;           ;__build__/debug/                                                                 ;IF NOT EXIST .\\__build__\\debug\\ MKDIR .\\__build__\\debug\\                                                        ;
Compile                ;main.c     ;src/main.c,__build__/debug/main.obj                                              ;cl.exe /nologo /c /FC /W4 /wd4201 /wd4456 /Od /Z7 /MT /DMTR_ENABLED /I./ /Fo:__build__/debug/                         ;src/main.c
                       ;thread.c   ;src/thread.c,__build__/debug/thread.obj                                          ;                                                                                                                      ;src/thread.c
                       ;minitrace.c;external/minitrace/minitrace.c,__build__/debug/minitrace.obj                     ;                                                                                                                      ;external/minitrace/minitrace.c
Link                   ;           ;__build__/debug/main.obj,__build__/debug/thread.obj,__build__/debug/minitrace.obj;cd __build__/debug/ & link.exe /nologo /DEBUG:FASTLINK /INCREMENTAL:NO main.obj thread.obj minitrace.obj /OUT:YABS.exe;
```
#### Graph
Each execution produces a **.trace** file located next to your **.build** file. It is a flame graph that can be displayed with most internet browsers (chrome://tracing, edge://tracing, ...)   
This should help you pinpoint what is taking up most of the time during your build.   
#### What's next 
- Maybe add an option so that YABS can generate a .build file to get people started faster.