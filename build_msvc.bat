@ECHO off

REM - - - - TIME - - - -
SETLOCAL EnableDelayedExpansion

SET "startTime=%time: =0%"
REM - - - - TIME - - - -




SET PATH_ROOT=%~dp0
SET PATH_SRC_DIR="%PATH_ROOT%src\"
SET PATH_EXTERNAL_DIR="%PATH_ROOT%external\"
SET PATH_BUILD_DIR="%PATH_ROOT%__build__\debug\\"

SET PATH_SOURCE="main.c" "thread.windows.c" "process.windows.c" "file_system.windows.c" "system.windows.c" "..\external\minitrace\minitrace.c"
SET PATH_OBJECT="main.obj" "thread.windows.obj" "process.windows.obj" "file_system.windows.obj" "system.windows.obj" "minitrace.obj"
SET NAME_OUTPUT=YABS.exe

SET DEFINES=/DMTR_ENABLED /DVERBOSE /D_CRT_SECURE_NO_WARNINGS

SET CFLAGS=/nologo /c /FC /W4 /wd4201 /wd4456 /Od /Z7 /MT /MP /Fo:%PATH_BUILD_DIR% /I"%PATH_ROOT%\" %DEFINES%
SET LFLAGS=/nologo /INCREMENTAL:NO /DEBUG:FASTLINK

IF NOT EXIST %PATH_BUILD_DIR% MKDIR %PATH_BUILD_DIR%

pushd %PATH_SRC_DIR%
cl.exe %CFLAGS% %PATH_SOURCE%
popd
pushd %PATH_BUILD_DIR%
link.exe %LFLAGS% %PATH_OBJECT% /OUT:%NAME_OUTPUT%
popd 
ROBOCOPY "%PATH_BUILD_DIR%" "%PATH_ROOT%\" "YABS.exe" > nul



REM - - - - TIME - - - -
SET "endTime=%time: =0%"

REM Get elapsed time:
SET "end=!endTime:%time:~8,1%=%%100)*100+1!"  &  SET "start=!startTime:%time:~8,1%=%%100)*100+1!"
SET /A "elap=((((10!end:%time:~2,1%=%%100)*60+1!%%100)-((((10!start:%time:~2,1%=%%100)*60+1!%%100)"

REM Convert elapsed time to HH:MM:SS:CC format:
SET /A "cc=elap%%100+100,elap/=100,ss=elap%%60+100,elap/=60,mm=elap%%60+100,hh=elap/60+100"

REM ECHO Start:    %startTime%
REM ECHO End:      %endTime%
ECHO Elapsed:  %hh:~1%%time:~2,1%%mm:~1%%time:~2,1%%ss:~1%%time:~8,1%%cc:~1%
REM - - - - TIME - - - -



REM - - - - DOCUMENTATION - - - -
	REM /c      Compile only, stop before linking.
	REM /nologo Suppresses the display of the copyright banner when the compiler starts up.
	REM /W4     Sets the warning level to 4.
	REM /wd4201 Suppresses a warning. Nonstandard extension used : nameless struct/union
	REM /wd4456 Suppresses a warning. The declaration of identifier in the local scope hides the declaration of the previous local declaration of the same name.
	REM /MT     Use the multithread, static version of the run-time library.
	REM /MP     Enables multi-process compilation.
	REM /Fo     Specifies an object (.obj) file name or directory to be used instead of the default.
	REM /Od     Disable all optimisations.
	REM /Z7     Generates less pdb files (implicitely triggers the generation of pdb files).
	REM /O2     Create the fastest code in the majority of cases.

	REM /nologo               Suppresses the display of the copyright banner when the compiler starts up.
	REM /SUBSYSTEM:WINDOWS    Specify the Windows subsystem targeted by the executable.
	REM /ENTRY:mainCRTStartup Specify a user-defined starting address for an .exe file or DLL.
	REM /INCREMENTAL:NO       Controls how the linker handles incremental linking.
	REM /OUT                  Specifies the output's name.
	REM /DEBUG:FASTLINK       Generate light .pdb info.
REM - - - - DOCUMENTATION - - - -