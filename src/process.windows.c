#include "process.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// PUBLIC FUNCTIONS
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
FILE*
ProcessStart(char* command)
{
	FILE* pipe;
	pipe = _popen(command, "r");
	return pipe;
}

int32_t
ProcessStop(FILE* pipe)
{
	return _pclose(pipe);
}