#include "system.h"

#define _AMD64_
#include <sysinfoapi.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// PUBLIC FUNCTIONS
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
uint32_t
SystemGetLogicalProcessorCount()
{
	SYSTEM_INFO systemInfo;
	GetSystemInfo(&systemInfo);
	return systemInfo.dwNumberOfProcessors;
}