#include "file_system.h"

#include "common.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// PUBLIC FUNCTIONS
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
char*
FileRead(char* filePath)
{
	FILE* file;
	errno_t error = fopen_s(&file, filePath, "rb");
	if (error != 0) return NULL;
	fseek(file, 0, SEEK_END);
	int64_t fileSizeBytes = ftell(file);
	fseek(file, 0, SEEK_SET);
	char* fileBuffer = (char*)malloc(fileSizeBytes + 1);
	fread(fileBuffer, 1, fileSizeBytes, file);
	fileBuffer[fileSizeBytes] = '\0';
	fclose(file);
	return fileBuffer;
}