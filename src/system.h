#pragma once

#include <stdint.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// API
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
uint32_t SystemGetLogicalProcessorCount();