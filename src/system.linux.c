#include "system.h"

#include <unistd.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// PUBLIC FUNCTIONS
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
uint32_t
SystemGetLogicalProcessorCount()
{
	return (uint32_t)sysconf(_SC_NPROCESSORS_ONLN);
}