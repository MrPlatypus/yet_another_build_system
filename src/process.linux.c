#include "process.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// PUBLIC FUNCTIONS
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
FILE*
ProcessStart(char* command)
{
	FILE* pipe;
	pipe = popen(command, "r");
	return pipe;
}

int32_t
ProcessStop(FILE* pipe)
{
	return pclose(pipe);
}