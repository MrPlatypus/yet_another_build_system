#include "thread.h"

#include "common.h"

#define _AMD64_
#include <windef.h>
#include <minwinbase.h>
#include <handleapi.h>
#include <processthreadsapi.h>
#include <synchapi.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// MACROS
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define INFINITE 0xFFFFFFFF

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// PUBLIC FUNCTIONS
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool
ThreadCreate(struct s_thread_instance* thread, uint64_t(functionPointer)(void*), void* param)
{
	thread->threadHandle = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)functionPointer, param, 0, 0);
	return SUCCESS;
}

void
ThreadPause(struct s_thread_instance* thread)
{
	SuspendThread(thread->threadHandle);
}

void
ThreadResume(struct s_thread_instance* thread)
{
	ResumeThread(thread->threadHandle);
}

void
ThreadJoin(struct s_thread_instance* thread)
{
	WaitForSingleObject(thread->threadHandle, INFINITE);
}

bool
ThreadDestroy(struct s_thread_instance* thread)
{
	TerminateThread(thread->threadHandle, 0);
	CloseHandle(thread->threadHandle);
	return SUCCESS;
}