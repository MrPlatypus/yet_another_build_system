#pragma once

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// MACROS
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define SUCCESS 1
#define FAILURE 0
#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1