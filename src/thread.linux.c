#include "thread.h"

#include "common.h"

#include <pthread.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TYPES
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
struct s_thread_function_wrapper
{
	uint64_t (*functionPointer)(void*);
	void* param;
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// PRIVATE FUNCTIONS
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void
ThreadWrapper(void* param)
{
	struct s_thread_function_wrapper* threadFunctionWrapper = (struct s_thread_function_wrapper*)(param);
	uint32_t returnValue = threadFunctionWrapper->functionPointer(threadFunctionWrapper->param);
	free(threadFunctionWrapper);
	pthread_exit((void*)returnValue);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// PUBLIC FUNCTIONS
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool
ThreadCreate(struct s_thread_instance* thread, uint64_t (*functionPointer)(void*), void* param)
{
	struct s_thread_function_wrapper* threadFunctionWrapper = (struct s_thread_function_wrapper*)malloc(sizeof(struct s_thread_function_wrapper));
	threadFunctionWrapper->functionPointer = functionPointer;
	threadFunctionWrapper->param = param;
	pthread_create(&thread->threadHandle, NULL, functionPointer, param);
}

void
ThreadPause(struct s_thread_instance* thread)
{
	// TODO
}

void
ThreadResume(struct s_thread_instance* thread)
{
	// TODO
}

void
ThreadJoin(struct s_thread_instance* thread)
{
	void* returnValue;
	pthread_join(thread->threadHandle, &returnValue);
}

bool
ThreadDestroy(struct s_thread_instance* thread)
{
	// Unecessary with pthread
	return SUCCESS;
}