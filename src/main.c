#include "common.h"

#include "file_system.h"
#include "process.h"
#include "thread.h"
#include "system.h"

#include <external/minitrace/minitrace.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// MACROS
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define CONFIG_COLLUMN_COUNT 5
#if defined(VERBOSE)
#define LOG(str) printf(str)
#define LOG_ARGS(str, ...) printf(str, __VA_ARGS__)
#else
#define LOG(str)
#define LOG_ARGS(str, ...)
#endif

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TYPES
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
struct s_job
{
	uint32_t threadIndex;
	volatile bool pending;
	volatile bool paused;
	volatile bool quit;
	char* stepTitle;
	char* taskTitle;
	char* command;
	char* argument;
};

struct s_csv_line
{
	union
	{
		char* columnPointers[CONFIG_COLLUMN_COUNT];
		struct {
			char* stepTitle;
			char* taskTitle;
			char* targets;
			char* command;
			char* argument; // If something is specified in this row then it will be appended to the command string
		};
	};
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// GLOBALS
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static time_t g_timestamp;
static bool g_taskFailed;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// FUNCTIONS
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
char*
ConcatString(char* base, char* addition)
{
	size_t baseLength = strlen(base);
	size_t additionLength = strlen(addition) + 1;
	char* result = malloc(baseLength + additionLength);
	memcpy(result, base, baseLength);
	memcpy(result + baseLength, addition, additionLength);
	return result;
}

THREADED_FUNCTION(Worker, param)
{
	struct s_job* job = (struct s_job*)param;

	size_t workerThreadNameSize = strlen("Worker Thread 00") + 1;
	char* workerThreadName = malloc(workerThreadNameSize);
	sprintf(workerThreadName, "Worker Thread %02i", job->threadIndex);
	MTR_META_THREAD_NAME(workerThreadName);
	free(workerThreadName);
	while (job->quit == false)
	{
		if (job->pending == true)
		{
			FILE* pipe;
			if (*job->argument != '\0')
			{
				MTR_BEGIN(job->stepTitle, "ConcatString");
				char* completeCommand = ConcatString(job->command, job->argument);
				MTR_END(job->stepTitle, "ConcatString");
				MTR_BEGIN(job->stepTitle, job->taskTitle);
				pipe = ProcessStart(completeCommand);
				char buffer[128];
				while (fgets(buffer, 128, pipe))
				{
					printf(buffer);
				}
				if (ProcessStop(pipe) != 0)
				{
					g_taskFailed = true;
				}
				MTR_END(job->stepTitle, job->taskTitle);
				free(completeCommand);
			}
			else
			{
				MTR_BEGIN(job->stepTitle, job->taskTitle);
				pipe = ProcessStart(job->command);
				char buffer[128];
				while (fgets(buffer, 128, pipe))
				{
					printf(buffer);
				}
				if (ProcessStop(pipe) != 0)
				{
					g_taskFailed = true;
				}
				MTR_END(job->stepTitle, job->taskTitle);
			}
			job->pending = false;
		}
	}
	return EXIT_SUCCESS;
}

time_t
GetFileTimestamp(char* path)
{
	struct stat attr; memset(&attr, 0, sizeof(attr));
	stat(path, &attr);
	return attr.st_mtime;
}

void
WaitForAllWorkToComplete(uint32_t threadCount, struct s_job* jobs, struct s_thread_instance* threads)
{
	for (uint32_t i = 0; i < threadCount;)
	{
		struct s_job* job = jobs + i;
		if (job->pending == false)
		{
			if (job->paused == false)
			{
				job->paused = true;
				ThreadPause(threads + i);
			}
			i += 1;
		}
	}
}

bool
GetNextLine(char** readPosition, struct s_csv_line* line)
{
	uint32_t columnIndex = 0;
	line->columnPointers[columnIndex++] = *readPosition;
	char c = *readPosition[0];
	while (c != '\r' && c != '\n' && c != '\0')
	{
		if (c == ';')
		{
			*readPosition[0] = '\0';
			line->columnPointers[columnIndex++] = (*readPosition) + 1;
		}
		c = *(++(*readPosition));
	}
	if (*readPosition[0] == '\n')
	{
		*readPosition[0] = '\0';
		*readPosition += 1;
	}
	if (*readPosition[0] == '\r')
	{
		*readPosition[0] = '\0';
		*readPosition += 2; // skipping \r and \n
	}
	return (columnIndex == CONFIG_COLLUMN_COUNT);
}

bool
CheckIfFileCanBeSkipped(char* path)
{
	time_t timestamp = GetFileTimestamp(path);
	// if path is empty or path doesn't exist or it's timestamp is newer than YABS's we won't skip this step.
	if (timestamp  == 0 || g_timestamp < timestamp)
	{
		LOG_ARGS("\t\t%s %s.\n", path, ((timestamp == 0) ? "doesn't exist" : "outdated"));
		return false;
	}
	LOG_ARGS("\t\t%s up-to-date.\n", path);
	return true;
}

bool
CheckIfTaskCanBeSkipped(char* fileList)
{
	char* readMarker = fileList;
	while (*fileList != '\0')
	{
		if (*fileList == ',')
		{
			*fileList = '\0';
			// if path is empty or path doesn't exist or it's timestamp is newer than YABS's we won't skip this step.
			if (CheckIfFileCanBeSkipped(readMarker) == false)
			{
				return false;
			}
			readMarker = fileList + 1;
		}
		++fileList;
	}
	return CheckIfFileCanBeSkipped(readMarker);
}

int32_t
CheckConfigFile(char* fileBuffer)
{
	char* readPosition = fileBuffer;

	// Checking if the number of seperators is a multiple of our collumn count
	{
		uint32_t seperatorCount = 0;
		uint32_t newlineCount = 0;
		while (*readPosition != '\0')
		{
			if (*readPosition == ';')
			{
				seperatorCount++;
			}
			else if (*readPosition == '\n')
			{
				newlineCount++;
				if (seperatorCount / newlineCount != (CONFIG_COLLUMN_COUNT - 1))
				{
					printf("[ERROR][l.%d] Incorrect number of seperators ';', there should be %i per line. %i were found.\n", newlineCount, CONFIG_COLLUMN_COUNT - 1, seperatorCount / newlineCount);
					return FAILURE;
				}
			}
			++readPosition;
		};
	}

	// In certain cases an empty "cell" is considered incorrect, this is what we are checking here
	{
		readPosition = fileBuffer;

		uint32_t lineCount = 0;
		struct s_csv_line line; memset(&line, 0, sizeof(line));
		struct s_csv_line linePrevious; memset(&linePrevious, 0, sizeof(linePrevious)); // Step and command columns can be "inherited"
		while (GetNextLine(&readPosition, &line))
		{
			if (line.stepTitle == 0 && linePrevious.stepTitle == 0)
			{
				printf("[ERROR][l.%i][c.0] Missing step title, please make sur that your first line has a step title.\n", lineCount);
				return FAILURE;
			}
			if (line.command == 0 && linePrevious.command == 0)
			{
				printf("[ERROR][l.%i][c.3] Missing command, please make sur that your first line has a command.\n", lineCount);
				return FAILURE;
			}
			if (line.stepTitle != 0 && line.command == 0)
			{
				printf("[ERROR][l.%i][c.3] New step title specified without a new command.\n", lineCount);
				return FAILURE;
			}

			// Saving column values for the next line if needed
			if (*line.stepTitle != '\0') linePrevious.stepTitle = line.stepTitle;
			if (*line.command != '\0') linePrevious.command = line.command;
			++lineCount;
		}
	}
	return SUCCESS;
}

int
main(int ac, char** av)
{
	if (ac < 2)
	{
		printf("Usage: YABS.exe project.build [--force, --check]");
		return EXIT_FAILURE;
	}

	bool force = false;
	bool checkConfigFile = false;
	if (ac >= 3)
	{
		if (strcmp(av[2], "-f") == 0|| strcmp(av[2], "--force") == 0)
		{
			force = true;
		}
		else if (strcmp(av[2], "-c") == 0 || strcmp(av[2], "--check") == 0)
		{
			checkConfigFile = true;
		}
	}

	if (checkConfigFile == true)
	{
		LOG("Checking config file...\n");
		char* fileBuffer;
		if ((fileBuffer = FileRead(av[1])) == NULL)
		{
			printf("Failed to read file \"%s\".\n", av[1]);
			return EXIT_FAILURE;
		}
		if (CheckConfigFile(fileBuffer) == SUCCESS)
		{
			printf("Config file is valid.");
		}
		free(fileBuffer);
		return EXIT_SUCCESS;
	}

	char* fileBuffer;
	char* traceFileName = ConcatString(av[1], ".trace");
	mtr_init(traceFileName);
	{
		MTR_BEGIN("Main", "Build");
		{
			MTR_META_THREAD_NAME("Main Thread");

			char* readPosition;
			MTR_BEGIN("Main", "Reading config file");
			LOG("Reading config file...\n");
			if ((fileBuffer = FileRead(av[1])) == NULL)
			{
				printf("Failed to read file \"%s\"\n", av[1]);
				mtr_shutdown();
				return EXIT_FAILURE;
			}
			readPosition = fileBuffer;
			MTR_END("Main", "Reading .build file");

			char* timeStampFileName = ConcatString(av[1], ".timestamp");
			g_timestamp = GetFileTimestamp(timeStampFileName);

			// If the config file has changed since the last build we check it just in case
			if (CheckIfFileCanBeSkipped(av[1]) == false)
			{
				MTR_BEGIN("Main", "Checking config file");
				LOG("Checking config file...\n");
				char* tempFileBuffer;
				if ((tempFileBuffer = FileRead(av[1])) == NULL)
				{
					printf("Failed to read file \"%s\"\n", av[1]);
					return EXIT_FAILURE;
				}
				if (CheckConfigFile(tempFileBuffer) == FAILURE)
				{
					printf("Exiting early.");
					return EXIT_FAILURE;
				}
				free(tempFileBuffer);
				MTR_END("Main", "Checking config file");
			}

			// Startup worker threads.
			MTR_BEGIN("Main", "Thread Startup");
			LOG("Thread startup...\n");
			const uint32_t threadCount = SystemGetLogicalProcessorCount();
			struct s_job* jobs = (struct s_job*)malloc(sizeof(struct s_job) * threadCount);
			struct s_thread_instance* threads = (struct s_thread_instance*)malloc(sizeof(struct s_thread_instance) * threadCount);;
			for (uint32_t i = 0; i < threadCount; ++i)
			{
				struct s_job* job = jobs + i;
				job->threadIndex = i;
				job->paused = true;
				job->quit = false;
				job->pending = false;
				job->stepTitle = NULL;
				job->taskTitle = NULL;
				job->command = NULL;
				job->argument = NULL;
				ThreadCreate(threads + i, Worker, (void*)(job));
				ThreadPause(threads + i);
				LOG_ARGS("\tThread #%i started.\n", i);
			}
			MTR_END("Main", "Thread Startup");

			struct s_csv_line line; memset(&line, 0, sizeof(line));
			struct s_csv_line linePrevious; memset(&linePrevious, 0, sizeof(linePrevious)); // Step and command columns can be "inherited"
			while (GetNextLine(&readPosition, &line))
			{
				if (line.stepTitle[0] != '\0')
				{
					if (g_taskFailed == false)
					{
						static bool first = true;
						if (first == false)
						{
							// Making sure everything is finished before moving on to the next step.
							WaitForAllWorkToComplete(threadCount, jobs, threads);
							MTR_END("Main", linePrevious.stepTitle);
						}
						else
						{
							first = false;
						}
						MTR_BEGIN("Main", line.stepTitle);
					}
					else
					{
						break; // We exit early as an error has occured
					}
				}
				LOG_ARGS("Task: %s%s\n", (*line.command == '\0') ? linePrevious.command : line.command, line.argument);

				// If no targets are specified then we always run the command.
				bool skip = false;
				if (force == false && line.targets[0] != '\0')
				{
					MTR_BEGIN(line.targets, "Checking if task can be skipped");
					LOG("\tChecking if task can be skipped...\n");
					skip = CheckIfTaskCanBeSkipped(line.targets);
					MTR_END(line.targets, "Checking if task can be skipped");
				}
				if (force == true || skip == false)
				{
					// Finding a thread that is available.
					for (uint32_t i = 0; i < threadCount;)
					{
						struct s_job* job = jobs + i;
						if (job->pending == false)
						{
							LOG_ARGS("Assigning task to thread #%i...\n", job->threadIndex);
							if (job->paused == true)
							{
								job->paused = false;
								ThreadResume(threads + i);
							}
							job->stepTitle = (*line.stepTitle == '\0') ? linePrevious.stepTitle : line.stepTitle;
							job->taskTitle = (*line.taskTitle == '\0') ? job->stepTitle : line.taskTitle;
							job->command = (*line.command == '\0') ? linePrevious.command : line.command;
							job->argument = line.argument;
							job->pending = true;
							break;
						}
						if (i == threadCount - 1)
						{
							i = 0;
						}
						else
						{
							++i;
						}
					}
				}
				else
				{
					LOG("Task skipped.\n");
				}
				// Saving column values for the next line if needed
				if (*line.stepTitle != '\0') linePrevious.stepTitle = line.stepTitle;
				if (*line.command != '\0') linePrevious.command = line.command;
			}
			WaitForAllWorkToComplete(threadCount, jobs, threads);
			MTR_END("Main", linePrevious.stepTitle);

			MTR_BEGIN("Main", "Thread Cleanup");
			LOG("Thread cleanup...\n");
			for (uint32_t i = 0; i < threadCount; ++i)
			{
				ThreadResume(threads + i);
				jobs[i].quit = true;
				ThreadJoin(threads + i);
				ThreadDestroy(threads + i);
				LOG_ARGS("\tThread #%i stopped.\n", i);
			}
			free(threads);
			free(jobs);
			MTR_END("Main", "Thread Cleanup");

			if (g_taskFailed == false)
			{
				MTR_BEGIN("Main", "Creating timestamp");
				LOG("Creating timestamp...\n");
				FILE* file = fopen(timeStampFileName, "w");
				fputc('\0', file);
				fclose(file);
				MTR_END("Main", "Creating timestamp");
			}
			free(timeStampFileName);
		}
		MTR_END("Main", "Build");
	}
	mtr_shutdown();
	free(traceFileName);
	free(fileBuffer);

	LOG_ARGS("Build %s.\n", ((g_taskFailed == true) ? "failed" : "succeeded"));
	return ((g_taskFailed == true) ? EXIT_FAILURE : EXIT_SUCCESS);
}