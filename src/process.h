#pragma once

#include <stdint.h>
#include <stdio.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// API
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
FILE*   ProcessStart(char* command);
int32_t ProcessStop(FILE* pipe);