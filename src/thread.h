#pragma once

#include <stdbool.h>
#include <stdint.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// MACROS
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define THREADED_FUNCTION(FunctionName, paramName) uint64_t FunctionName(void* paramName)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TYPES
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
struct s_thread_instance
{
	void* threadHandle;
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// API
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool ThreadCreate(struct s_thread_instance* thread, uint64_t (*functionPointer)(void*), void* param);
void ThreadPause(struct s_thread_instance* thread);
void ThreadResume(struct s_thread_instance* thread);
void ThreadJoin(struct s_thread_instance* thread);
bool ThreadDestroy(struct s_thread_instance* thread);